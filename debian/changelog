erlang-p1-cache-tab (1.0.31-2) unstable; urgency=medium

  * Updated Standards-Version: 4.7.0 (no changes needed)
  * Updated years in debian/copyright
  * debian/rules: add creation of symbolic link for native app name

 -- Philipp Huebner <debalance@debian.org>  Sun, 09 Feb 2025 10:33:59 +0100

erlang-p1-cache-tab (1.0.31-1) unstable; urgency=medium

  * New upstream version 1.0.31
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Thu, 03 Oct 2024 17:14:49 +0200

erlang-p1-cache-tab (1.0.30-3) unstable; urgency=medium

  * Dropped debian/erlang-p1-cache-tab.install, added debian/source/options
    and fixed debian/rules (closes: #1044515, #1049689)
  * Updated debian/source/lintian-overrides
  * Renamed debian/erlang-p1-cache-tab.lintian-overrides
    -> debian/lintian-overrides

 -- Philipp Huebner <debalance@debian.org>  Tue, 26 Dec 2023 15:57:25 +0100

erlang-p1-cache-tab (1.0.30-2) unstable; urgency=medium

  * Updated Standards-Version: 4.6.2 (no changes needed)
  * Updated Erlang dependencies
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Wed, 08 Feb 2023 11:08:47 +0100

erlang-p1-cache-tab (1.0.30-1) unstable; urgency=medium

  * New upstream version 1.0.30
  * Updated Standards-Version: 4.6.1 (no changes needed)
  * Updated Erlang dependencies
  * Updated years in debian/copyright
  * Refreshed patches

 -- Philipp Huebner <debalance@debian.org>  Thu, 02 Jun 2022 09:47:00 +0200

erlang-p1-cache-tab (1.0.29-2) unstable; urgency=medium

  * Updated debian/watch

 -- Philipp Huebner <debalance@debian.org>  Sat, 18 Dec 2021 12:35:23 +0100

erlang-p1-cache-tab (1.0.29-1) unstable; urgency=medium

  * New upstream version 1.0.29
  * Updated Standards-Version: 4.6.0 (no changes needed)
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sun, 29 Aug 2021 20:04:39 +0200

erlang-p1-cache-tab (1.0.27-2) unstable; urgency=medium

  * Corrected Multi-Arch setting to "allowed"

 -- Philipp Huebner <debalance@debian.org>  Sun, 31 Jan 2021 18:13:02 +0100

erlang-p1-cache-tab (1.0.27-1) unstable; urgency=medium

  * New upstream version 1.0.27
  * Added 'Multi-Arch: same' in debian/control
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Sat, 30 Jan 2021 16:28:24 +0100

erlang-p1-cache-tab (1.0.26-1) unstable; urgency=medium

  * New upstream version 1.0.26
  * Updated Standards-Version: 4.5.1 (no changes needed)
  * Updated Erlang dependencies
  * Updated version of debian/watch: 4
  * Updated debhelper compat version: 13
  * Updated lintian overrides
  * Added debian/erlang-p1-cache-tab.install

 -- Philipp Huebner <debalance@debian.org>  Fri, 25 Dec 2020 22:24:25 +0100

erlang-p1-cache-tab (1.0.25-1) unstable; urgency=medium

  * New upstream version 1.0.25
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sun, 02 Aug 2020 16:39:33 +0200

erlang-p1-cache-tab (1.0.24-1) unstable; urgency=medium

  * New upstream version 1.0.24
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sun, 26 Jul 2020 21:00:23 +0200

erlang-p1-cache-tab (1.0.22-1) unstable; urgency=medium

  * New upstream version 1.0.22
  * Updated Erlang dependencies
  * Refreshed debian/patches/mips-atomic.diff

 -- Philipp Huebner <debalance@debian.org>  Tue, 17 Mar 2020 16:35:54 +0100

erlang-p1-cache-tab (1.0.21-1) unstable; urgency=medium

  * New upstream version 1.0.21
  * Fixed typo in long package description.
  * Updated Standards-Version: 4.5.0 (no changes needed)
  * Updated Erlang dependencies
  * Updated years in debian/copyright
  * Rules-Requires-Root: no

 -- Philipp Huebner <debalance@debian.org>  Tue, 04 Feb 2020 21:30:20 +0100

erlang-p1-cache-tab (1.0.20-1) unstable; urgency=medium

  * New upstream version 1.0.20
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sat, 10 Aug 2019 13:55:22 +0200

erlang-p1-cache-tab (1.0.19-1) unstable; urgency=medium

  * New upstream version 1.0.19
  * Updated Erlang dependencies
  * Updated Standards-Version: 4.4.0 (no changes needed)
  * debian/rules: export ERL_COMPILER_OPTIONS=deterministic
  * Updated debhelper compat version: 12

 -- Philipp Huebner <debalance@debian.org>  Wed, 24 Jul 2019 11:56:41 +0200

erlang-p1-cache-tab (1.0.17-1) unstable; urgency=medium

  * New upstream version 1.0.17
  * Updated Standards-Version: 4.3.0 (no changes needed)
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Tue, 01 Jan 2019 22:33:46 +0100

erlang-p1-cache-tab (1.0.16-1) unstable; urgency=medium

  * New upstream version 1.0.16
  * Enabled DH_VERBOSE in debian/rules
  * Updated Standards-Version: 4.2.1 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sun, 07 Oct 2018 14:57:25 +0200

erlang-p1-cache-tab (1.0.14-1) unstable; urgency=medium

  * New upstream version 1.0.14
  * Added debian/upstream/metadata
  * Updated Standards-Version: 4.1.4 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Wed, 04 Jul 2018 16:23:45 +0200

erlang-p1-cache-tab (1.0.13-1) unstable; urgency=medium

  * New upstream version 1.0.13
  * Use secure copyright format uri in debian/copyright
  * Switched to debhelper 11

 -- Philipp Huebner <debalance@debian.org>  Tue, 27 Mar 2018 22:30:25 +0200

erlang-p1-cache-tab (1.0.12-2) unstable; urgency=medium

  * Set Maintainer: Ejabberd Packaging Team <ejabberd@packages.debian.org>
  * Set Uploaders: Philipp Huebner <debalance@debian.org>
  * Updated Vcs-* fields in debian/control for salsa.debian.org
  * Updated years in debian/copyright
  * Updated Standards-Version: 4.1.3 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Thu, 04 Jan 2018 11:37:47 +0100

erlang-p1-cache-tab (1.0.12-1) unstable; urgency=medium

  * New upstream version 1.0.12
  * (Build-)Depend on erlang-base (>= 1:19.2)
  * Updated Standards-Version: 4.1.1 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Mon, 27 Nov 2017 19:47:49 +0100

erlang-p1-cache-tab (1.0.10-1) unstable; urgency=medium

  * New upstream version 1.0.10
  * Updated Standards-Version: 4.1.0 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sat, 02 Sep 2017 16:57:32 +0200

erlang-p1-cache-tab (1.0.9-2) unstable; urgency=medium

  * Added patch to fix FTBFS on mips, mipsel and powerpc (Closes: #868787)
    Thanks James Cowgill <jcowgill@debian.org>!

 -- Philipp Huebner <debalance@debian.org>  Wed, 19 Jul 2017 14:41:00 +0200

erlang-p1-cache-tab (1.0.9-1) unstable; urgency=medium

  * New upstream version 1.0.9
  * Updated Standards-Version: 4.0.0 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Tue, 11 Jul 2017 12:02:31 +0200

erlang-p1-cache-tab (1.0.7-1~exp1) experimental; urgency=medium

  * New upstream version 1.0.7
  * Updated debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Sat, 15 Apr 2017 12:22:56 +0200

erlang-p1-cache-tab (1.0.4-2) unstable; urgency=medium

  * Added erlang-base to Build-Depends

 -- Philipp Huebner <debalance@debian.org>  Wed, 05 Oct 2016 15:57:35 +0200

erlang-p1-cache-tab (1.0.4-1) unstable; urgency=medium

  * New upstream version 1.0.4
  * (Build-)Depend on erlang-p1-utils (>= 1.0.5)

 -- Philipp Huebner <debalance@debian.org>  Fri, 16 Sep 2016 14:29:42 +0200

erlang-p1-cache-tab (1.0.3-1) unstable; urgency=medium

  * Imported Upstream version 1.0.3

 -- Philipp Huebner <debalance@debian.org>  Sun, 03 Jul 2016 16:28:22 +0200

erlang-p1-cache-tab (1.0.2-3) unstable; urgency=medium

  * Improved debian/watch
  * Updated Standards-Version: 3.9.8 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Mon, 30 May 2016 20:14:52 +0200

erlang-p1-cache-tab (1.0.2-2) unstable; urgency=medium

  * Updated debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Wed, 17 Feb 2016 20:32:34 +0100

erlang-p1-cache-tab (1.0.2-1) unstable; urgency=medium

  * Imported Upstream version 1.0.2
  * Updated Standards-Version: 3.9.7 (no changes needed)
  * Updated Vcs-* fields in debian/control

 -- Philipp Huebner <debalance@debian.org>  Tue, 02 Feb 2016 18:54:36 +0100

erlang-p1-cache-tab (1.0.1-1) unstable; urgency=medium

  * Imported Upstream version 1.0.1
  * Updated debian/watch
  * Updated debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Thu, 14 Jan 2016 11:03:19 +0100

erlang-p1-cache-tab (0.2015.07.28-1) unstable; urgency=medium

  * Imported Upstream version 0.2015.07.28
  * Enabled eunit

 -- Philipp Huebner <debalance@debian.org>  Tue, 18 Aug 2015 07:59:36 +0200

erlang-p1-cache-tab (0.2015.03.31-2) unstable; urgency=medium

  * Streamlined debian/rules
  * Fixed debian/copyright (make lintian happy)
  * Fixed FTBFS

 -- Philipp Huebner <debalance@debian.org>  Sat, 02 May 2015 14:50:10 +0200

erlang-p1-cache-tab (0.2015.03.31-1) unstable; urgency=medium

  * Imported Upstream version 0.2015.03.31
  * Updated Standards-Version: 3.9.6 (no changes needed)
  * Added Vcs links to debian/control
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Tue, 28 Apr 2015 16:08:01 +0200

erlang-p1-cache-tab (0.2014.07.17-1) unstable; urgency=medium

  * Imported Upstream version 0.2014.07.17

 -- Philipp Huebner <debalance@debian.org>  Thu, 28 Aug 2014 11:49:26 +0200

erlang-p1-cache-tab (0.2014.04.30-1) unstable; urgency=medium

  * Imported Upstream version 0.2014.04.30

 -- Philipp Huebner <debalance@debian.org>  Thu, 10 Jul 2014 17:33:17 +0200

erlang-p1-cache-tab (0.2013.05.15-2) unstable; urgency=medium

  * Add correct Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sat, 10 May 2014 00:25:10 +0200

erlang-p1-cache-tab (0.2013.05.15-1) unstable; urgency=low

  * Initial release, see #744885 for background

 -- Philipp Huebner <debalance@debian.org>  Wed, 16 Apr 2014 18:33:55 +0200
